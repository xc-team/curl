<?php

namespace XcTeam\Curl;

class NetworkRequest
{
    private static $curl = null;
    private static $requestData;

    /**
     * 设置请求数据
     * @param string $key 键
     * @param string $value 值
     * @return void
     */
    public static function setRequestData($key, $value)
    {
        self::$requestData[$key] = $value;
    }

    public static function getRequestData()
    {
        return self::$requestData;
    }

    public static function use(callable $callback = null)
    {
        self::initCurl();
        if (is_callable($callback)) {
            $callback(new RequestMode, new Config);
            return self::sendRequest();
        }
    }

    private static function initCurl()
    {
        self::$curl = curl_init();
        curl_setopt(self::$curl, CURLOPT_FOLLOWLOCATION, Config::getFollowStatus());
        curl_setopt(self::$curl, CURLOPT_USERAGENT, Config::getUserAgent());
        curl_setopt(self::$curl, CURLOPT_AUTOREFERER, true);
        curl_setopt(self::$curl, CURLOPT_TIMEOUT, Config::getTimeOut());
        curl_setopt(self::$curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(self::$curl, CURLOPT_HEADER, true);
        if ($port = (Config::getPort() !== 0))
            curl_setopt(self::$curl, CURLOPT_PORT, $port);
        curl_setopt(self::$curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt(self::$curl, CURLOPT_SSL_VERIFYHOST, false);
        if ($cookie = (Config::getCookie() == ''))
            curl_setopt(self::$curl, CURLOPT_COOKIE, $cookie);
        if ($header = (Config::getReferer() == ''))
            curl_setopt(self::$curl, CURLOPT_REFERER, $header);
    }

    private static function sendRequest()
    {
        foreach (self::$requestData as $key => $value) {
            curl_setopt(self::$curl,  eval('return ' . $key . ';'), $value);
        }
        if (count(Config::getHeaderContent()))
            curl_setopt(self::$curl, CURLOPT_HTTPHEADER, Config::getHeaderContent());
        $content = curl_exec(self::$curl);
        $status = curl_getinfo(self::$curl, CURLINFO_HTTP_CODE);
        $error = curl_error(self::$curl);
        curl_close(self::$curl);
        $resultArr = explode(PHP_EOL . PHP_EOL, $content);
        list($headerInfo, $content) = array($resultArr[count($resultArr) - 2], $resultArr[count($resultArr) - 1]);
        foreach (explode(PHP_EOL, $headerInfo) as $header) {
            $headerArr = explode(':', $header);
            count($headerArr) == 2 and $headers[] = array($headerArr[0] => $headerArr[1]);
        }
        return array(
            'status' => $status,
            'headers' => isset($headers) ? $headers : array(),
            'content' => $content,
            'error' => $error
        );
    }
}
