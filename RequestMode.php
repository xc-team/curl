<?php

namespace XcTeam\Curl;

use DefaultSetting;

class RequestMode
{
    public static function get($url, $param = array())
    {
        if (count($param)) $url .= '?' . http_build_query($param);
        NetworkRequest::setRequestData('CURLOPT_URL', $url);
    }

    public static function post($url, $data)
    {
        NetworkRequest::setRequestData('CURLOPT_URL', $url);
        NetworkRequest::setRequestData('CURLOPT_POST', true);
        NetworkRequest::setRequestData('CURLOPT_POSTFIELDS', $data);
    }

    public static function patch($url, $data)
    {
        NetworkRequest::setRequestData('CURLOPT_CUSTOMREQUEST', 'PATCH');
        NetworkRequest::setRequestData('CURLOPT_URL', $url);
        NetworkRequest::setRequestData('CURLOPT_POSTFIELDS', $data);
    }

    public static function put($url, $data)
    {
        NetworkRequest::setRequestData('CURLOPT_CUSTOMREQUEST', 'PUT');
        NetworkRequest::setRequestData('CURLOPT_URL', $url);
        NetworkRequest::setRequestData('CURLOPT_POSTFIELDS', $data);
    }

    public static function delete($url, $data)
    {
        NetworkRequest::setRequestData('CURLOPT_CUSTOMREQUEST', 'DELETE');
        NetworkRequest::setRequestData('CURLOPT_URL', $url);
        NetworkRequest::setRequestData('CURLOPT_POSTFIELDS', $data);
    }
}
