# curl

## 安装
### composer require xc-team/curl

``` php
//使用方式
$result = NetworkRequest::use(function ($requestMode, $config) {
    // $config 等同于 new Config
    // $requestMode 等同于 new RequestMode
    $requestMode->post('http://xxx.com/xxx', ['a' => 'b']); //第二个参数data 可以为数组和对象，当执行sendJsonData等转换数据类型的函数则自动转换
    DefaultSetting::sendJsonData();
});
```

Config类
    可自定义设置请求配置
RequestMode类
    预设请求方式
DefaultSetting类
    预设请求配置

以上自行查阅代码注释
