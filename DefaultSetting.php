<?php

namespace XcTeam\Curl;

use XcTeam\Curl\Config;

class DefaultSetting
{
    /**
     * 设置curl请求数据为json格式
     * @return void
     */
    public static function sendJsonData()
    {
        Config::add(['Content-Type' => 'application/json; charset=UTF-8']);
        $requestData = NetworkRequest::getRequestData();
        if ($requestData['CURLOPT_POSTFIELDS']) {
            if (is_object($requestData['CURLOPT_POSTFIELDS']) || is_array($requestData['CURLOPT_POSTFIELDS']))
                NetworkRequest::setRequestData('CURLOPT_POSTFIELDS', json_encode($requestData['CURLOPT_POSTFIELDS']));
        }
    }
}
