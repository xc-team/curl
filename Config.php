<?php

namespace XcTeam\Curl;

/**
 * CURL配置类
 * @author mahaibo <mahaibo@hongbang.js.cn> 
 */
class Config
{
    private static $curlConfig; //curl配置

    /**
     * 开启自动重定向跟踪
     * @return void
     */
    public static function openFollow()
    {
        self::$curlConfig['is_follow'] = true;
    }

    /**
     * 获取设置跟随状态,未设置默认为开启
     * @return bool 开启活关闭
     */
    public static function getFollowStatus()
    {
        return self::$curlConfig['is_follow'] ?? true;
    }

    /**
     * 设置用户代理方式
     * @param string $userAgent 用户代理方式
     * @return void
     */
    public static function setUserAgent($userAgent)
    {
        self::$curlConfig['user_agent'] = $userAgent;
    }

    /**
     * 获取用户代理
     * @return string 代理方式
     */
    public static function getUserAgent()
    {
        return self::$curlConfig['user_agent'] ?? $_SERVER['HTTP_USER_AGENT'];
    }

    /**
     * 设置超时时间
     * @param integer $sec 单位秒
     * @return void
     */
    public static function setTimeout(int $sec)
    {
        self::$curlConfig['time_out'] = $sec;
    }

    /**
     * 获取超时时间,默认20秒
     * @return integer 超时时间
     */
    public static function getTimeOut()
    {
        return self::$curlConfig['time_out'] ?? 20;
    }

    /**
     * 设置Cookie
     * @param array $cookies cookie
     * @return void
     */
    public static function setCookie(array $cookies)
    {
        $cookieArr = array();
        foreach ($cookies as $key => $value) {
            $cookieArr[] = $key . '=' . $value;
        }
        self::$curlConfig['cookies'] = implode('; ', $cookieArr);
    }

    /**
     * 获取Cookie
     * @return string cookie
     */
    public static function getCookie()
    {
        return self::$curlConfig['cookies'] ?? '';
    }

    /**
     * 设置来源
     * @param string $referer 来源
     * @return void
     */
    public static function setReferer($referer)
    {
        self::$curlConfig['referer'] = $referer;
    }

    /**
     * 获取来源
     * @return string referer
     */
    public static function getReferer()
    {
        return self::$curlConfig['referer'] ?? '';
    }

    /**
     * 设置端口
     * @return integer 端口
     */
    public static function setPort($port)
    {
        self::$curlConfig['port'] = $port;
    }

    /**
     * 获取端口
     * @return integer 端口
     */
    public static function getPort()
    {
        return self::$curlConfig['port'] ?? 0;
    }

    /**
     * 追加header
     * @param mixed ...$param 自定义参数
     * @example add('key','value') 单一追加 key键,value值
     * @example add(['key' => 'value']) 批量追加 key键,value值
     * @return self
     * @author mahaibo <mahaibo@hongbang.js.cn>
     */
    public static function add(...$param)
    {
        foreach ($param as $key => $value) {
            if (is_string($value) && is_string($param[$key + 1])) {
                self::$curlConfig['headers'][self::formatKey($value)] = $param[$key + 1];
                break;
            }
            if (is_array($value)) {
                foreach ($value as $k => $val) {
                    if (is_string($val)) {
                        self::$curlConfig['headers'][self::formatKey($k)] = $val;
                    }
                }
            }
        }
    }

    /**
     * 设置Header,覆盖设置
     * @param array $param set(['key' => 'value'])批量追加 key键,value值
     * @return self
     * @author mahaibo <mahaibo@hongbang.js.cn>
     */
    public static function set(array $param = array())
    {
        self::$curlConfig['headers'] = array();
        foreach ($param as $key => $value) {
            self::$curlConfig['headers'][self::formatKey($key)] = $value;
        }
    }

    /**
     * 获取最终结果
     * @return array
     * @author mahaibo <mahaibo@hongbang.js.cn>
     */
    public static function getHeaderContent(): array
    {
        $headers = array();
        if (self::$curlConfig['headers']) foreach (self::$curlConfig['headers'] as $key => $value) {
            $headers[] = $key . ': ' . $value;
        }
        return $headers;
    }

    /**
     * 格式化Header Key
     * @param string $key
     * @return string
     * @author mahaibo <mahaibo@hongbang.js.cn>
     */
    private static function formatKey($key): string
    {
        $key = str_replace(['_'], ['-'], $key);
        $key = strtolower($key);
        $keyArr = explode('-', $key);
        foreach ($keyArr as $k => $v) {
            $keyArr[$k] = ucfirst($v);
        }
        return implode('-', $keyArr);
    }
}
